# README #

Composer library to easily create forms in Wordpress admin environment.

### Installation

Add this to your composer.json:
```
    "repositories": [
        {
            "type": "vcs",
            "url": "git@bitbucket.org:perfacilis/wp-form.git"
        }
    ]
```

After saving, add the requirement:
```
composer require perfacilis/wp-form:dev-master
```

After that it can be used in php:

```
use Perfacilis/WpForm/Form;
use Perfacilis/WpForm/Fieldset;
use Perfacilis/WpForm/Text;
use Perfacilis/WpForm/Button;

$form = new Form('test');
$form->addFieldset(new Fieldset());

$namefield = new Text('name', __('Your name:'));
$namefield->setValue('');
$form->add($namefield);

$form->add(new Button('save', __('Save')));

if (!$form->isPosted()) {
  return;
}

$invalid_fields = $form->validate();
if ($invalid_fields) {
  // ... Show notice
  return;
}

$postdata = $form->getData();
$username = $postdata['name'];
```
