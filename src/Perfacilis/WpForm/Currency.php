<?php

/*
 * @author Roy Arisse <support@perfacilis.com>
 * @copyright (c) 2019, Perfacilis
 */
namespace Perfacilis\WpForm;

class Currency extends Number
{
    public function __construct($name, $label = '', $min = null, $max = null, $step = null)
    {
        if ($min === null) {
            $min = 0;
        }

        if ($step === null) {
            $step = .01;
        }

        parent::__construct($name, $label, $min, $max, $step);
        $this->attributes['placeholder'] = '0.00';
    }

    public function setValue($value)
    {
        $value = isset($_POST[$this->name]) ? $_POST[$this->name] : $value;
        $this->value = (float) $value;

        return true;
    }

    public function isValid(): bool
    {
        if ($this->isRequired()) {
            return $this->value > 0;
        }

        return true;
    }

    public function getHtml(): string
    {
        $html = $this->getLabelHtml();

        $html .= '      <div class="input-group">' . PHP_EOL;
        $html .= '        <div class="input-group-prepend">' . PHP_EOL;
        $html .= '          <span class="input-group-text">&euro;</span>' . PHP_EOL;
        $html .= '        </div> ' . PHP_EOL;

        $this->attributes['value'] = money_format('%i', $this->value);
        $html .= '        <input' . Form::printAttributes($this->attributes) . ' />' . PHP_EOL;

        $html .= '      </div>' . PHP_EOL;

        return $html;
    }
}
