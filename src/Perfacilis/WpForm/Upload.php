<?php

/*
 * @author Roy Arisse <support@perfacilis.com>
 * @copyright (c) 2019, Perfacilis
 */
namespace Perfacilis\WpForm;

class Upload extends Formfield
{

    private $accept_mimes = array();

    public function __construct($name, $label = '')
    {
        parent::__construct($name, $label);

        $this->accept_mimes = array();

        $this->attributes['size'] = 40;
    }

    public function addAllowedMime($extensions)
    {
        if (!is_array($extensions)) {
            $extensions = array($extensions);
        }

        foreach ($extensions as $ext) {
            $this->accept_mimes[$ext] = $ext;
        }

        return true;
    }

    public function imageOnly($only_allow_image = true)
    {
        return $this->accept_mimes = !$only_allow_image ? array() : array('image/*');
    }

    public function setValue($value)
    {
        $value = isset($_FILES[$this->name]) ? $_FILES[$this->name]['name'] : $value;
        return $this->value = $value;
    }

    public function isValid()
    {
        if (!$this->isRequired()) {
            return true;
        }

        return !empty($_FILES[$this->name]) && empty($_FILES[$this->name]['error']);
    }

    public function getHtml()
    {
        $html = $this->getLabelHtml();

        $this->attributes['type'] = 'file';
        if (!empty($this->accept_mimes)) {
            $this->attributes['accept'] = implode(',', array_unique($this->accept_mimes));
        }

        $this->attributes['value'] = htmlentities($this->value);

        $html .= '					<input' . Form::printAttributes($this->attributes) . ' />' . PHP_EOL;

        return $html;
    }

}
