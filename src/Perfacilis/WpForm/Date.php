<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Perfacilis\WpForm;

/**
 * Description of Date
 *
 * @author roy
 */
class Date extends Formfield
{

    //put your code here
    public function getHtml()
    {
        $html = $this->getLabelHtml();

        $this->attributes['type'] = 'date';
        $this->attributes['value'] = htmlentities($this->value);

        $html .= '					<input' . Form::printAttributes($this->attributes) . ' />' . PHP_EOL;

        return $html;
    }

    public function setValue($value)
    {
        if (isset($_POST[$this->name])) {
            $value = $_POST[$this->name];
        }

        if (is_numeric($value)) {
            $value = date('Y-m-d', $value);
        }

        $this->value = $value;
    }

    public function getValue()
    {
        $value = $this->value;
        if (!is_numeric($value)) {
            $value = strtotime($value);
        }

        return $value;
    }
}
