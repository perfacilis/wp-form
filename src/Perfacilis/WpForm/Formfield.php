<?php

/*
 * @author Roy Arisse <support@perfacilis.com>
 * @copyright (c) 2019, Perfacilis
 */
namespace Perfacilis\WpForm;

abstract class FormField
{
    protected $label = '';
    protected $attributes = array();
    protected $is_required = false;
    protected $name = '';
    protected $value = '';
    protected $skip_for = false;
    protected $labelattributes = array();
    protected $label_suffix = '';
    protected $break_count = 0;
    protected $posted = false;
    protected $hints = [];

    abstract public function getHtml();

    public function __construct($name, $label = '')
    {
        $this->attributes['id'] = self::sanitizeString($name);
        $this->name = self::sanitizeString($name);
        $this->attributes['name'] = $this->name;

        return $this->label = $label;
    }

    public function setLabelSuffix($suffix)
    {
        return $this->label_suffix = $suffix;
    }

    public function getType()
    {
        return isset($this->attributes['type']) ? $this->attributes['type'] : 'text';
    }

    public function getName()
    {
        return $this->name;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setPosted($isposted = true)
    {
        return $this->posted = $isposted;
    }

    /*
     * @param mixed $value
     */

    public function setValue($value)
    {
        $value = isset($_POST[$this->name]) ? $_POST[$this->name] : $value;

        $class = get_called_class();
        switch ($class) {
            default:
                $this->value = $value;
                break;

            case 'F_Checkbox':
                $this->value = !empty($_POST) ? isset($_POST[$this->name]) : func_get_arg(0);
                break;
        }

        return true;
    }

    public function getValue()
    {
        $class = get_called_class();
        switch ($class) {
            default:
                // Remove Wordpress's added slashes (weirdo's)
                $value = stripslashes_deep($this->value);
                break;
        }

        return $value;
    }

    public function setRequired($is_required = true)
    {
        unset($this->attributes['required']);
        if ($is_required) {
            $this->attributes['required'] = null;
        }

        return $this->is_required = $is_required;
    }

    public function addAttribute($attr, $value, $append = true)
    {
        if ($append && is_string($value) && isset($this->attributes[$attr])) {
            $value = $this->attributes[$attr] . ' ' . $value;
        }

        if ($attr == 'disabled') {
            $this->addAttribute('class', 'disabled');
        }

        return $this->attributes[$attr] = $value;
    }

    public function addLabelAttribute($attr, $value)
    {
        if (is_string($value) && isset($this->labelattributes[$attr])) {
            $value = $this->labelattributes[$attr] . ' ' . $value;
        }

        return $this->labelattributes[$attr] = $value;
    }

    public function setBreaks($amount_of_br)
    {
        return $this->break_count = (int) $amount_of_br;
    }

    public function isRequired()
    {
        return $this->is_required;
    }

    public function getAttributes()
    {
        return Form::printAttributes($this->attributes);
    }

    public function setInvalid()
    {
        $this->addLabelAttribute('class', 'invalid');
        $this->addAttribute('class', 'invalid');
    }

    public function isValid()
    {
        if (!$this->isRequired()) {
            return true;
        }

        return !empty($_POST[$this->name]);
    }

    public function getStartingHtml()
    {
        $html = '            <tr' . ($this->is_required ? ' class="form-required"' : '') . '>' . PHP_EOL;
        if (!$this->label) {
            $html .= '              <td colspan="2">' . PHP_EOL;
        }

        return $html;
    }

    public function getEndinghtml()
    {
        $html = '';
        foreach ($this->hints as $hint) {
            $html .= '					<p class="hint"><em><small>' . $hint . '</small></em></p>' . PHP_EOL;
        }

        for ($i = 0; $i < $this->break_count; $i += 1) {
            $html .= '					<br />' . PHP_EOL;
        }

        $html .= '</td></tr>' . PHP_EOL;
        return $html;
    }

    public function addHint($hint)
    {
        $this->hints[] = $hint;
    }

    protected function getLabelHtml()
    {
        if (empty($this->label)) {
            return '';
        }

        if ($this->posted && !$this->isValid()) {
            $this->addLabelAttribute('class', 'invalid');
            $this->addAttribute('class', 'invalid');
        }

        if (!$this->skip_for) {
            $this->labelattributes['for'] = $this->attributes['id'];
        }

        $html = PHP_EOL;
        $html .= '            <th scope="row">' . PHP_EOL;
        $html .= '				<label' . Form::printAttributes($this->labelattributes) . '>' . $this->label;

        if ($this->is_required) {
            $html .= '<span class="required"> *</span>';
        }

        $html .= $this->label_suffix . '</label>' . PHP_EOL;

        // End col for label, start col for field
        $html .= '            </th><td>' . PHP_EOL;

        return $html;
    }

    private static function sanitizeString($string)
    {
        return preg_replace('~[^\w\d\[\]\-\_]+~', '', $string);
    }
}
