/*
 * @author Roy Arisse <support@perfacilis.com>
 * @copyright (c) 2019, Perfacilis
 */
(function ($) {
    'use strict';
    $.fn.Populator = function () {
        var $input = $(this),
            $options = {
                template: '#template',
                target: '#target',
                button: '#button',
                counter: 999,
                onadd: $.noop(/* item, value */)
            };

        $.extend($options, $input.data());

        // Add item if button is clicked
        $($options.button).on('click', addItem.bind(this));

        // Pass <enter>-key to button-click
        $input.on('keypress', function (e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                $($options.button).click();
            }
        });

        function addItem () {
            var value = $input.val(),
                $template = $($($options.template).html().trim()),
                $target = $($options.target),
                isTinyMce = $input.data('tinymce');

            if (isTinyMce) {
                value = wp.editor.getContent($input.attr('id'));
            }

            if (!value.length) {
                return;
            }

            // Set up template
            $template.find('[data-source]').each(function () {
                var $el = $(this);
                switch (this.tagName.toLowerCase()) {
                    case 'input':
                    case 'select':
                        $el.val(value);
                        $el.attr('name',
                            $el.attr('name').replace('[]', '[' + $options.counter + ']'));
                        break;

                    default:
                        $el.html(value);
                        break;
                }
            });

            // Run add-trigger
            $input.trigger('populator.add', [$template, value]);

            $options.counter += 1;

            // Clear value and add template
            if (isTinyMce) {
                let editor = window.tinymce.get($input.attr('id'));
                editor.setContent('');
            } else {
                $input.val('');
            }

            $target.append($template);

            // Assign delete button
            assignDeleteButtons();
        }

        function assignDeleteButtons () {
            $($options.target).find('.submitdelete')
                .off('click')
                .on('click', function () {
                    $(this).parent().remove();
                });
        }

        // Ensure pre-added items can be removed too
        assignDeleteButtons();
    };

    $('[data-populator]').Populator();
})(jQuery);