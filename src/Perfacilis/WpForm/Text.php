<?php

/*
 * @author Roy Arisse <support@perfacilis.com>
 * @copyright (c) 2019, Perfacilis
 */
namespace Perfacilis\WpForm;

class Text extends Formfield
{
    public function __construct($name, $label = '')
    {
        parent::__construct($name, $label);
        $this->attributes['type'] = 'text';
        $this->attributes['class'] = 'regular-text';
    }

    public function setTiny()
    {
        $this->attributes['class'] = 'tiny-text';
    }

    public function setSmall()
    {
        $this->attributes['class'] = 'small-text';
    }

    public function setLarge()
    {
        $this->attributes['class'] = 'large-text';
    }

    public function setReadOnly($is_readonly = true)
    {
        if ($is_readonly) {
            $this->addAttribute('readonly', null, false);
        } elseif (isset($this->attributes['readonly'])) {
            unset($this->attributes['readonly']);
        }
    }

    public function getHtml()
    {
        $html = $this->getLabelHtml();

        $this->attributes['value'] = htmlentities($this->value);
        $html .= '					<input' . Form::printAttributes($this->attributes) . ' />' . PHP_EOL;

        return $html;
    }
}
