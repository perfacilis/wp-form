<?php

/*
 * @author Roy Arisse <support@perfacilis.com>
 * @copyright (c) 2019, Perfacilis
 */
namespace Perfacilis\WpForm;

class Radio extends Formfield
{

    public function __construct($name, $label = '')
    {
        parent::__construct($name, $label);
        $this->attributes['type'] = 'radio';
    }

    public function addOption($title, $value = null, $disabled = false)
    {
        return $this->options[] = array(
            'title' => $title,
            'value' => $value !== null ? $value : $title,
            'disabled' => $disabled,
        );
    }

    public function getHtml()
    {
        $html = $this->getLabelHtml();

        foreach ($this->options as $i => $option) {
            $attrs = $this->attributes;
            $attrs['id'] .= $i;
            $attrs['value'] = $option['value'];
            if ($option['value'] == $this->value) {
                $attrs['checked'] = 'checked';
            }

            $html .= '<input' . Form::printAttributes($attrs) . ' />' . PHP_EOL;
            $html .= '<label for="' . $attrs['id'] . '">' . $option['title'] . '</label>' . PHP_EOL;
            $html .= '<br />' . PHP_EOL;
        }

        return $html;
    }

    private $options = array();

}
