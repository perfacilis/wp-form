<?php

/*
 * @author Roy Arisse <support@perfacilis.com>
 * @copyright (c) 2019, Perfacilis
 */
namespace Perfacilis\WpForm;

class Form
{
    public function __construct($name, $action = null)
    {
        $this->action = $action !== null ? (string) $action : null;

        $this->attributes['accept-encoding'] = 'utf-8';
        $this->attributes['name'] = $name;

        wp_enqueue_style(__NAMESPACE__, plugin_dir_url(__FILE__) . 'style.min.css');
        wp_enqueue_script(__NAMESPACE__, plugin_dir_url(__FILE__) . 'jquery.collapse.min.js');
    }

    public function addAttribute($attr, $value)
    {
        return $this->attributes[$attr] = $value;
    }

    public function addFieldset(Fieldset $obfieldset)
    {
        return $this->fieldsets[] = $obfieldset;
    }

    public function setMethod($method)
    {
        return $this->method = $method == 'get' ? 'get' : 'post';
    }

    public function add($ob_field_or_string)
    {
        $currentfieldset = count($this->fieldsets) - 1;

        $fields = func_get_args();
        foreach ($fields as $ob_field_or_string) {
            $class = is_object($ob_field_or_string) ? str_replace(__NAMESPACE__ . '\\', '', get_class($ob_field_or_string)) : 'string';
            if ($class == 'Fieldset') {
                $this->fieldsets[] = $ob_field_or_string;
                $currentfieldset += 1;
                continue;
            }

            if (stripos($class, 'Upload') !== false) {
                $this->attributes['enctype'] = 'multipart/form-data';
            }

            $this->fields[] = array(
                'field' => $ob_field_or_string,
                'fieldset' => $currentfieldset,
            );
        }

        return true;
    }

    public function __toString()
    {
        if ($this->method) {
            $this->attributes['method'] = $this->method;
        }

        if ($this->action !== null) {
            $this->attributes['action'] = $this->action;
        }

        $is_posted = $this->isPosted();
        $eol = PHP_EOL;
        $html = ''
            . '				<form' . Form::printAttributes($this->attributes) . '>' . PHP_EOL;

        /**
         * Make postboxes work
         * @see https://code.tutsplus.com/articles/integrating-with-wordpress-ui-meta-boxes-on-custom-pages--wp-26843
         */
        $html .= ''
            . wp_nonce_field('meta-box-order', 'meta-box-order-nonce', false, false)
            . wp_nonce_field('closedpostboxes', 'closedpostboxesnonce', false, false)
            . '               <div id="poststuff">' . PHP_EOL
            . '                 <div id="side-sortables" class="meta-box-sortables ui-sortable">' . PHP_EOL;

        $lastfieldset = null;
        foreach ($this->fields as $field) {
            if ($lastfieldset !== $field['fieldset']) {
                if ($lastfieldset !== null) {
                    $html .= $this->fieldsets[$field['fieldset']]->getCloseHtml($lastfieldset > 0);
                }

                $html .= $this->fieldsets[$field['fieldset']]->getOpenHtml();
                $lastfieldset = $field['fieldset'];
            }

            if (is_string($field['field'])) {
                $html .= $field['field'];
                continue;
            }

            // Set field as posted
            $field['field']->setPosted($is_posted);

            $html .= $field['field']->getStartingHtml();
            $html .= $field['field']->getHtml();
            $html .= $field['field']->getEndingHtml();
        }

        if ($lastfieldset !== null) {
            $html .= $this->fieldsets[$lastfieldset]->getCloseHtml(true);
        }

        $html .= ''
            . '                  </div>' . PHP_EOL
            . '                </div>' . PHP_EOL
            . '				</form>' . PHP_EOL;

        return $html;
    }

    public static function printAttributes($attr_array)
    {
        $attrs = array();
        foreach ($attr_array as $attr => $value) {
            $attrs[] = $attr . ($value !== null ? '="' . $value . '"' : '');
        }

        return ' ' . implode(' ', $attrs);
    }

    public function validate()
    {
        if ($this->invalidfields) {
            return $this->invalidfields;
        }

        $this->invalidfields = array();
        foreach (array_keys($this->fields) as $id_field) {
            $field = &$this->fields[$id_field]['field'];

            if (!is_string($field) && !$field->isValid()) {
                $this->invalidfields[$field->getName()] = $field->getLabel();
            }
        }

        return $this->invalidfields;
    }

    /**
     * @return array like $_POST
     * @todo Make field[foo][bar] ect work as well and validate input, since its not validatet now
     */
    public function getData()
    {
        $data = array();
        foreach (array_keys($this->fields) as $id_field) {
            if (is_string($this->fields[$id_field]['field'])) {
                continue;
            }

            $value = $this->fields[$id_field]['field']->getValue();
            if ($value === null) {
                continue;
            }

            $name = $this->fields[$id_field]['field']->getName();
            $data[$name] = $value;

            if (strpos($name, '[') === false || strpos($name, ']') === false) {
                continue;
            }

            // !!! Single level works properly, multi level not yet
            // Field is array, add to result as well
            $key = preg_replace('~\[\w+\]+~', '', $name);
            if (isset($_POST[$key]) && !isset($data[$key])) {
                $data[$key] = (array) $_POST[$key];
            }
        }

        return $data;
    }

    public function isPosted()
    {
        return strtolower($_SERVER['REQUEST_METHOD']) == $this->method;
    }

    protected $action = null;
    protected $method = 'post';
    protected $attributes = array();
    protected $fields = array();

    /* non overrideable */
    private $fieldsets = array();

    /* self controlling */
    private $invalidfields = array();
}
