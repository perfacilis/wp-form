<?php

/*
 * @author Roy Arisse <support@perfacilis.com>
 * @copyright (c) 2019, Perfacilis
 */
namespace Perfacilis\WpForm;

class Fieldset
{
    private $legend = '';
    private $legendattributes = [];
    private $attributes = [
        'class' => 'postbox',
    ];

    public function __construct($legend = '')
    {
        static $counter = 0;

        $this->addAttribute('id', 'fieldset' . $counter++, false);
        $this->legend = $legend;
        $this->setCollapsable(is_admin());
    }

    public function getLegend()
    {
        return $this->legend;
    }

    public function addAttribute($attr, $value, $append = true)
    {
        if ($append && isset($this->attributes[$attr])) {
            $value = $this->attributes[$attr] . ' ' . $value;
        }

        return $this->attributes[$attr] = $value;
    }

    public function addLegendAttribute($attr, $value, $append = true)
    {
        if ($append && isset($this->legendattributes[$attr])) {
            $value = $this->legendattributes[$attr] . ' ' . $value;
        }

        return $this->legendattributes[$attr] = $value;
    }

    public function setCollapsable(bool $is_collapsable = true): void
    {
        if ($this->collapsable) {
            return;
        }

        $this->collapsable = $is_collapsable;
        $this->addLegendAttribute('class', 'hndle');
        $this->addLegendAttribute('data-toggle', 'collapse', false);
        $this->addLegendAttribute('data-target', '#' . $this->attributes['id'] . ' .inside', false);
    }

    public function setAccordion(bool $is_collapsed = true): void
    {
        $this->setCollapsable(true);
        $this->addLegendAttribute('data-parent', 'form', false);

        $this->accordion_collapsed = $is_collapsed;
        if ($is_collapsed) {
            $this->addLegendAttribute('class', 'collapsed');
        }
    }

    public function getOpenHtml(): string
    {
        $html = ''
            . '          <section' . Form::printAttributes($this->attributes) . '>' . PHP_EOL;

        $legend = $this->getLegend();

        if ($legend) {
            $htag = is_admin() ? 'h2' : 'h3';
            $html .= ''
                . '            <' . $htag . Form::printAttributes($this->legendattributes) . '>' . PHP_EOL
                . '              <span>' . $this->legend . '</span>' . PHP_EOL
                . '            </' . $htag . '>' . PHP_EOL;
        }

        $html .= ''
            . '            <div class="inside ' . ($this->accordion_collapsed ? 'collapse' : 'collapse show') . '">' . PHP_EOL
            . '              <table class="form-table">' . PHP_EOL;

        return $html;
    }

    public function getCloseHtml(): string
    {
        $html = ''
            . '              </table>' . PHP_EOL
            . '            </div>' . PHP_EOL
            . '          </section>';

        return $html;
    }

    private $collapsable = false;
    private $accordion_collapsed = false;
}
