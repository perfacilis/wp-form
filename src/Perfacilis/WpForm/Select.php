<?php

/*
 * @author Roy Arisse <support@perfacilis.com>
 * @copyright (c) 2019, Perfacilis
 */
namespace Perfacilis\WpForm;

class Select extends Formfield
{
    const TYPE_OPTION = 'option';
    const TYPE_GROUP = 'optgroup';

    private $options = array();

    public function addOptGroup($title)
    {
        return $this->options[] = [
            'title' => $title,
            'type' => self::TYPE_GROUP,
            'value' => null,
        ];
    }

    public function addOption($title, $value = null, array $attributes = [])
    {
        return $this->options[] = array_merge($attributes, [
            'title' => $title,
            'type' => self::TYPE_OPTION,
            'value' => $value !== null ? $value : $title,
        ]);
    }

    /**
     * Add multiple options at once
     * @param array $options Formatted like [value => title, ...]
     * @return void
     */
    public function addOptionArray(array $options): void
    {
        foreach ($options as $value => $title) {
            $this->addOption($title, $value);
        }
    }

    public function getHtml()
    {
        $eol = "\r\n";
        $html = $this->getLabelHtml();

        $class = 'select-container';
        if (isset($this->attributes['class'])) {
            $class .= ' ' . $this->attributes['class'];
            unset($this->attributes['class']);
        }

        $html .= '              <div class="' . $class . '">' . $eol;
        $html .= '                <select' . Form::printAttributes($this->attributes) . '>' . $eol;

        $optgroup_open = false;
        foreach ($this->options as $option) {
            if ($option['type'] == self::TYPE_GROUP) {
                if ($optgroup_open) {
                    $html .= '                  </optgroup>' . $eol;
                }

                $html .= '                  <optgroup label="' . $option['title'] . '">' . $eol;
                $optgroup_open = true;
            } else {
                $label = $option['title'];
                $attrs = array_filter($option, function ($k) {
                    return !in_array($k, ['title', 'type']);
                }, ARRAY_FILTER_USE_KEY);

                if ($option['value'] == $this->value) {
                    $attrs['selected'] = null;
                }

                $html .= ($optgroup_open ? '  ' : '')
                    . '                  <option' . Form::printAttributes($attrs) . '>' . $option['title'] . '</option>' . PHP_EOL;
            }
        }

        if ($optgroup_open) {
            $html .= '                  </optgroup>' . $eol;
        }

        $html .= '                </select>' . $eol;
        $html .= '              </div>' . $eol;

        return $html;
    }
}
