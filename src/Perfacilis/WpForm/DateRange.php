<?php

namespace Perfacilis\WpForm;

/**
 * Description of DateRange
 *
 * @author roy
 */
class DateRange extends Formfield
{
    public function __construct($name, $label = '', $second_label = 't/m')
    {
        parent::__construct($name, $label);
        $this->second_label = $second_label;
    }

    public function getHtml()
    {
        $html = $this->getLabelHtml();

        for ($i = 0; $i < 2; $i += 1) {
            if ($i == 1 && $this->second_label) {
                $html .= '  <span>' . $this->second_label . '</span>';
            }

            $this->attributes['type'] = 'date';
            $this->attributes['value'] = htmlentities($this->value[$i]);
            $this->attributes['name'] = $this->name . '[' . $i . ']';

            $html .= '					<input' . Form::printAttributes($this->attributes) . ' />' . PHP_EOL;
        }

        return $html;
    }

    public function setValue($value)
    {
        if (isset($_POST[$this->name])) {
            $value = (array) $_POST[$this->name];
        }

        foreach ($value as $k => $v) {
            if (is_numeric($v)) {
                $value[$k] = date('Y-m-d', $v);
            }
        }

        $this->value = $value;
    }

    public function getValue()
    {
        $value = $this->value;
        foreach ($value as $k => $v) {
            if (!is_numeric($v)) {
                $value[$k] = strtotime($v);
            }
        }

        return $value;
    }

    private $second_label = '';
}
