<?php

/*
 * @author Roy Arisse <support@perfacilis.com>
 * @copyright (c) 2019, Perfacilis
 */
namespace Perfacilis\WpForm;

class Checkbox extends Formfield
{
    public function __construct($name, $label = '')
    {
        parent::__construct($name, $label);

        $this->attributes['type'] = 'checkbox';
        $this->attributes['class'] = 'checkbox';
        $this->label_suffix = '';
        $this->value = [];
    }

    public function addOption($title, $value = null, array $attr = [])
    {
        $attr['title'] = $title;
        $attr['value'] = $value !== null ? $value : $title;
        $this->options[] = $attr;
    }

    public function getHtml()
    {
        $html = $this->getLabelHtml();

        foreach ($this->options as $i => $option) {
            $attrs = array_merge($option, $this->attributes);
            $attrs['id'] .= $i;
            $attrs['name'] .= '[]';

            if (in_array($option['value'], $this->value)) {
                $attrs['checked'] = 'checked';
            }

            $html .= '<input' . Form::printAttributes($attrs) . ' />' . PHP_EOL;
            $html .= '<label for="' . $attrs['id'] . '">' . $option['title'] . '</label>' . PHP_EOL;
            $html .= '<br />' . PHP_EOL;
            $html .= PHP_EOL;
        }

        return $html;
    }

    /**
     * @param array $value
     */
    public function setValue($value)
    {
        if (!empty($_POST)) {
            $value = isset($_POST[$this->name]) ? $_POST[$this->name] : [];
        }

        $this->value = (array) $value;

        return true;
    }

    private $options = array();
}
