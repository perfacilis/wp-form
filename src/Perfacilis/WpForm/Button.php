<?php

/*
 * @author Roy Arisse <support@perfacilis.com>
 * @copyright (c) 2019, Perfacilis
 */
namespace Perfacilis\WpForm;

class Button extends Formfield
{
    protected $attributes = array(
        'class' => 'button',
        'type' => 'button',
    );

    public function getHtml()
    {
        $html = '               <td colspan="2">' . PHP_EOL;
        $html .= '					<button' . Form::printAttributes($this->attributes) . '><span>' . $this->label . '</span></button>' . PHP_EOL;

        return $html;
    }

    public function isDeleteButton()
    {
        $this->addAttribute('class', 'button-link-delete');
    }

    public function isValid()
    {
        return true;
    }
}
