<?php

/*
 * @author Roy Arisse <support@perfacilis.com>
 * @copyright (c) 2019, Perfacilis
 */
namespace Perfacilis\WpForm;

class Textarea extends Formfield
{

    public function __construct($name, $label = '')
    {
        parent::__construct($name, $label);

        $this->attributes['rows'] = 5;
        $this->attributes['cols'] = 50;
        $this->attributes['class'] = 'large-text';
    }

    public function getHtml()
    {
        $eol = "\r\n";
        $html = $this->getLabelHtml();
        $html .= '					<textarea' . Form::printAttributes($this->attributes) . '>' . $this->value . '</textarea>' . $eol;

        return $html;
    }

}
