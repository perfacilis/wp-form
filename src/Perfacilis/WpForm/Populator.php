<?php

/*
 * @author Roy Arisse <support@perfacilis.com>
 * @copyright (c) 2019, Perfacilis
 */
namespace Perfacilis\WpForm;

class Populator extends FormField
{
    public function getHtml()
    {
        // We need to alter the attributes a bit
        $attributes = $this->attributes;
        $attributes['type'] = 'text';
        unset($attributes['class'], $attributes['name']);

        // Define unique id's
        $buttonId = $this->attributes['id'] . 'Button';
        $templateId = $this->attributes['id'] . 'Template';
        $targetId = $this->attributes['id'] . 'Target';

        // Add attributes for JS' Populator plugin
        $attributes['data-populator'] = null;
        $attributes['data-template'] = '#' . $templateId;
        $attributes['data-target'] = '#' . $targetId;
        $attributes['data-button'] = '#' . $buttonId;

        // Start output
        $html = $this->getLabelHtml();

        // Container for options
        $html .= ''
            . '<div class="postbox regular-text" style="width: auto;">' . PHP_EOL
            . '  <div class="submitbox" id="' . $targetId . '">' . PHP_EOL;

        foreach ($this->value as $item_id => $item_name) {
            $html .= $this->getItemTamplate($item_name, $item_id);
        }

        $html .= ''
            . '  </div>' . PHP_EOL
            . '</div>' . PHP_EOL;

        // Add answer field
        if ($this->editor) {
            $html .= Editor::getMediaButtonsHtml($this->attributes['id']);
            $html .= Editor::getTextareaHtml($this->attributes['id'], $attributes);
        } else {
            $html .= '<input' . Form::printAttributes($attributes) . ' />' . PHP_EOL;
        }

        $html .= '<button class="button" type="button" id="' . $buttonId . '">Toevoegen &raquo;</button>' . PHP_EOL;

        // Template for new options
        $html .= ''
            . '<template id="' . $templateId . '">' . PHP_EOL
            . $this->getItemTamplate('') . PHP_EOL
            . '</template>' . PHP_EOL;

        // Required javascript to get it to work
        $html .= '<script src="' . plugin_dir_url(__FILE__) . 'Populator.js"></script>';

        return $html;
    }

    /**
     * Enable (minimal) RTF/WYSIWYG editor for answers
     */
    public function enableEditor(bool $enabled = true): void
    {
        $this->editor = $enabled;
    }

    /**
     * @param array $value
     * @return boolean
     */
    public function setValue($value)
    {
        $value = isset($_POST[$this->name]) ? $_POST[$this->name] : $value;
        $this->value = (array) $value;

        return true;
    }

    public function getValue()
    {
        $value = parent::getValue();
        if (!is_array($value) && $value) {
            $value = explode(self::DELIMITER, $value);
        }

        return (array) $value;
    }

    /**
     * Set the HTML-template used to render an item.
     * Possible replacements are:
     * - %%name%% will be replaced by fieldname[id]
     * - %%id%% will be replaced by item id (if set)
     * - %%value%% will be replaced by item value
     *
     * @param string $template
     * @return void
     */
    public function setItemTemplate($template)
    {
        $this->template = $template;
    }

    private $template = ''
        . '  <div class="misc-pub-section">' . PHP_EOL
        . '    <a class="submitdelete" href="javascript:void(null);">&times;</a>' . PHP_EOL
        . '    <span data-source="title">%%value%%</span>' . PHP_EOL
        . '    <input type="hidden" data-source="title" name="%%name%%" value="%%value%%" />' . PHP_EOL
        . '  </div>' . PHP_EOL;
    private $editor = false;

    private function getItemTamplate($item_name, $item_id = null)
    {
        $search = [
            '%%name%%',
            '%%id%%',
            '%%value%%',
        ];

        $replace = [
            $this->attributes['name'] . '[' . ($item_id !== null ? $item_id : '') . ']',
            $item_id !== null ? $item_id : '',
            $item_name,
        ];

        return str_replace($search, $replace, $this->template);
    }
}
