<?php

/*
 * @author Roy Arisse <support@perfacilis.com>
 * @copyright (c) 2019, Perfacilis
 */
namespace Perfacilis\WpForm;

class ImageUpload extends Upload
{

    public function __construct($name, $label = '')
    {
        parent::__construct($name, $label);
        $this->imageOnly();
    }

    public function getHtml()
    {
        $html = parent::getHtml();

        if ($this->value) {
            $html .= '<img src="' . $this->value . '"'
                . ' style="max-height: 50px; max-width: 100px; border: 1px solid #ddd;" />';
        }

        return $html;
    }

}
