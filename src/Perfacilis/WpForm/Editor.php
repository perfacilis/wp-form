<?php

namespace Perfacilis\WpForm;

/**
 * Description of Editor
 *
 * @author roy <support@perfacilis.com>
 */
class Editor extends Textarea
{
    public function getHtml()
    {
        $html = $this->getLabelHtml();

        $html .= self::getMediaButtonsHtml($this->attributes['id']);
        $html .= self::getTextareaHtml($this->attributes['id'], $this->attributes, $this->value);

        return $html;
    }

    public static function getTextareaHtml($id, array $attributes = [], string $value = '')
    {
        $attributes['id'] = $id;
        $attributes['data-tinymce'] = true;

        // Add input and button to add new items
        $html = '<div id="wp-' . $id . '-editor-container" class="wp-editor-container">' . PHP_EOL;
        $html .= '  <textarea' . Form::printAttributes($attributes) . ' />' . htmlentities($value) . '</textarea>' . PHP_EOL;
        $html .= '</div>' . PHP_EOL;

        // Create editor js
        wp_enqueue_editor();

        $html .= '<script>
jQuery(document).ready(function() {
    wp.editor.initialize(\'' . $id . '\');
});</script>' . PHP_EOL;

        return $html;
    }

    public static function getMediaButtonsHtml($id)
    {
        $html = '<div id="wp-' . $id . '-media-buttons" class="wp-media-buttons">' . PHP_EOL;

        ob_start();
        media_buttons($id);
        $html .= '  ' . ob_get_contents() . PHP_EOL;
        ob_end_clean();

        $html .= "</div>" . PHP_EOL;

        return $html;
    }
}
