<?php

/*
 * @author Roy Arisse <support@perfacilis.com>
 * @copyright (c) 2019, Perfacilis
 */
namespace Perfacilis\WpForm;

class Submit extends Button
{

    protected $attributes = array(
        'class' => 'button',
        'type' => 'submit',
    );

    public function getHtml()
    {
        // Make sure btn-primary, btn-default or anything like that has been set
        if (strpos($this->attributes['class'], 'button-') === false) {
            $this->attributes['class'] .= ' button-primary button-large';
        }

        return parent::getHtml();
    }

}
