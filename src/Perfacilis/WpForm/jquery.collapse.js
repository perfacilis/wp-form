/*
 * @author Roy Arisse <roy@perfacilis.com>
 * @copyright (c) 2019, Perfacilis
 */
(function ($) {
    'use strict';

    $.fn.Collapse = function () {
        var $this = $(this),
            $target = $($this.data('target')),
            $parent = $this.data('parent') ? $this.closest($this.data('parent')) : [],
            checkbox = $this.attr('type') && $this.attr('type').indexOf('checkbox') > -1;

        function show() {
            hideOthers();
            $this.removeClass('collapsed');
            $target.addClass('show');
        }

        function hide() {
            $this.addClass('collapsed');
            $target.removeClass('show');
        }

        function hideOthers() {
            if (!$parent.length) {
                return;
            }

            $parent.find('[data-toggle=collapse]').each(function() {
                let $other = $(this);

                if ($this !== $other) {
                    $other.addClass('collapsed');
                    $($other.data('target')).removeClass('show');
                }
            });
        }

        if (checkbox) {
            $this.change(function () {
                if ($this.is(':checked')) {
                    show();
                } else {
                    hide();
                }
            });
            $this.change();
        } else {
            $this.click(function () {
                if ($this.hasClass('collapsed')) {
                    show();
                } else {
                    hide();
                }
            });
        }

    };

    $(document).ready(function () {
        $('[data-toggle=collapse]').each(function () {
            $(this).Collapse();
        });
    });
})(jQuery);