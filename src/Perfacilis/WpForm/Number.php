<?php

/*
 * @author Roy Arisse <support@perfacilis.com>
 * @copyright (c) 2019, Perfacilis
 */
namespace Perfacilis\WpForm;

class Number extends Text
{
    public function __construct($name, $label = '', $min = null, $max = null, $step = null)
    {
        parent::__construct($name, $label);
        $this->attributes['type'] = 'number';
        $this->addAttribute('class', 'number');

        if (is_numeric($min)) {
            $this->addAttribute('min', $min);
        }

        if (is_numeric($max)) {
            $this->addAttribute('max', $max);
        }

        if (is_numeric($step)) {
            $this->addAttribute('step', $step);
        }
    }

    public function setValue($value)
    {
        if (isset($this->attributes['min']) && $value < $this->attributes['min']) {
            $value = $this->attributes['min'];
        }

        if (isset($this->attributes['max']) && $value > $this->attributes['max']) {
            $value = $this->attributes['max'];
        }

        return parent::setValue($value);
    }
}
